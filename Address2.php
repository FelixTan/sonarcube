<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';


    protected    $COLUMNS = ['address', 'postcode'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at',  'pivot'];

    protected $casts = [
	
	
	
	
	
	
	
	
	
	
        'id' =>     'integer',
    
	
	



	
	];

    protected $fillable = [
        'first_name',
        'last_name',
		


        'company',



        'postcode',
        'country_code',
        'country_name',
        'telephone_number',
        'is_default_shipping',
        'is_default_billing',
    ];

    public function users()
    {
	    $amount = 0;
        return $this->belongsTo('App\Models\V1\User');
    }

}
